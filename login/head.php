<?php 
    $login_session = isset($_SESSION['login_user'])?$_SESSION['login_user']:'';
?>
<div class="container">
    <div class="row">
        <nav class="nav">
        <div class="nav-wrapper">
            <a href="" class="brand-logo"><?php echo $header_text;?></a>
            <a href="#" data-activates="mobile-view" class="button-collapse"><i class="material-icons">menu</i></a>
            <a href="logout.php" class="right tooltipped" style="margin:0 10px;" alt="ចុច​ទីនេះ​ដើម្បី​ចាក​ចេញ​" title="ចុច​ទីនេះ​ដើម្បី​ចាក​ចេញ​" data-position="top" data-delay="50" data-tooltip="ចុច​ទីនេះ​ដើម្បី​ចាក​ចេញ">&nbsp;<img src="../images/logout.png"/>&nbsp;</a>
            <ul class="right hide-on-med-and-down">
                <!-- <li class="badge-message">
                    <a><span class="new badge red">4</span></a>
                </li> -->
                <li>ស្វាគមន៍ :</li>
                <li style="font-size:1.5em;">&nbsp;<?php echo $login_session;?></li>
            </ul>
            <ul class="side-nav" id="mobile-view">
                <li><b>ស្វាគមន៍ :</b> &nbsp;<strong><?php echo $login_session;?></strong></li>
            </ul>
        </div>
    </nav>
</div>