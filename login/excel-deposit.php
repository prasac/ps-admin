<?php 
// error_reporting(0);
$objPHPExcel->setActiveSheetIndex(0);
$stylehead = array(
  'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
  ),
  'font'  => array(
      'bold'  => true,
      'color' => array('rgb' => '4DB848'),
      'size'  => 16,
      'name'  => 'Khmer OS Muol Light'
  )
);

$style = array(
  'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
  ),
  'font'  => array(
      'bold'  => true,
      'color' => array('rgb' => 'ffffff'),
      'size'  => 14,
      'name'  => 'Khmer OS Content'
  ),
  'borders' => array(
    'allborders' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('rgb' => '449d44')
    )
  ),
);
$styledate = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font'  => array(
        'bold'  => false,
        'color' => array('rgb' => '449d44'),
        'size'  =>12,
        'name'  => 'Khmer OS Content'
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'E7E7E7')
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '449d44')
        )
    )
);
$styletitle = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font'  => array(
        'bold'  => false,
        'color' => array('rgb' => '449d44'),
        'size'  =>14,
        'name'  => 'Khmer OS Muol Light'
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'E7E7E7')
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('rgb' => '449d44')
        )
    )
);
$stylecontent = array(
  'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
  ),
  'font'  => array(
      'bold'  => false,
      'size'  => 13,
      'name'  => 'Khmer OS Content'
  )
);
$stylecontentright = array(
  'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
  ),
  'font'  => array(
      'bold'  => false,
      'size'  => 14,
      'name'  => 'Khmer OS Content'
  ),
  'borders' => array(
    'allborders' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('rgb' => '449d44')
    )
),
);

$BStyle = array(
    'borders' => array(
      'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('rgb' => '449d44')
      )
    )
  );
 
$query = "SELECT
d.id,d.account_name_kh,d.account_name_latin,d.account_number,d.account_type,d.currency_type,d.services,d.service_other,d.resident_status,
p.dob dob,p.gender,p.nationality,p.relationship,p.name,p.name_latin,p.type_of_id ,p.id_number,p.occupation,p.institution,p.is_us,
p.id_issured_date,p.id_expiry_date,p.phone_number,p.email_address,p.address,b.name_kh branch_name,d.is_view,d.created_date
FROM deposit_customer_info AS d
inner join deposit_customer_personal_info as p on p.customer_id=d.id
left join branches as b on d.duty_station=d.id
where d.id in($items_checked) and p.relationship IS NULL
ORDER BY d.created_date DESC ";
$results = $conn->query($query);

$account_type_list = array(1=>'គណនីសន្សំ / Saving  Account',2=>'គណនីចម្រើន​គ្មាន​កាល​កំណត់ / Unfixed Deposit Account',3=>'គណនីសោធននិវត្តន៍ / Retirement Account', 4=>'ផ្សេងៗ / Others');
$currency_type_list = array(1=>'ប្រាក់​រៀល / KHR',2=>'ប្រាក់​ដុល្លា / USD',3=>'ប្រាក់​បាត / THB',4=>'ផ្សេងៗ / Others');
$services_list = array(1=>'ប័ណ្ណអេធីអឹម / ATM Card',2=>'សេវាធនាគារចល័ត / Mobile and Internet Banking',3=>'ផ្សេងៗ / Others');
$resient_status_list = array(1=>'និវាសនជន / Resident',2=>'អនិវាសនជន / Non-Resident');

if($results->num_rows){
  $n=5;
  $i=0;
  while($row = mysqli_fetch_assoc($results)){   
    // echo $i.'<br />';
    // $objPHPExcel->getActiveSheet(0);
    $objWorkSheet = $objPHPExcel->createSheet($i); //Setting index when creating

    $gdImage = '../images/logo-website.png';
    $img = @imagecreatefrompng($gdImage);
    imagealphablending($img, true);
    imagesavealpha($img, true);
    $header = "image/png";

    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
    $objDrawing->setName('Prasac Logo');
    $objDrawing->setDescription('Prasac Logo');
    $objDrawing->setImageResource($img);
    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    $objDrawing->setCoordinates('A1');
    $objDrawing->setWorksheet($objWorkSheet);


    $objWorkSheet->mergeCells('A1:H1');
    $objWorkSheet->getRowDimension('1')->setRowHeight(100);
    $objWorkSheet->getCell('A1')->setValue("ពាក្យ​ស្នើសុំ​បើក​គណនី\nACCOUNT OPENING APPLICATION​​");
    $objWorkSheet->getStyle("A1:H1")->applyFromArray($stylehead);
    $objWorkSheet->getStyle('A1')->getAlignment()->setWrapText(true);
    $objWorkSheet->mergeCells('A2:H2');
    $objWorkSheet->mergeCells('A4:H4');
    $objWorkSheet->mergeCells('A12:H12');
    $objWorkSheet->mergeCells('B5:HF5');
    $objWorkSheet->mergeCells('B6:H6');
    $objWorkSheet->mergeCells('B7:H7');
    $objWorkSheet->mergeCells('B8:H8');
    $objWorkSheet->mergeCells('B9:H9');
    $objWorkSheet->mergeCells('B10:H10');
    $objWorkSheet->mergeCells('B11:H11');

    $objWorkSheet->mergeCells('B13:H13');
    $objWorkSheet->mergeCells('B14:H14');
    $objWorkSheet->mergeCells('B15:H15');
    $objWorkSheet->mergeCells('B16:H16');
    $objWorkSheet->mergeCells('B17:H17');
    $objWorkSheet->mergeCells('B18:H18');
    $objWorkSheet->mergeCells('B19:H19');
    $objWorkSheet->mergeCells('B20:H20');
    $objWorkSheet->mergeCells('B21:H21');
    $objWorkSheet->mergeCells('B22:H22');
    $objWorkSheet->mergeCells('B23:H23');
    $objWorkSheet->mergeCells('B24:H24');
    $objWorkSheet->mergeCells('B25:H25');
    $objWorkSheet->mergeCells('B26:H26');
    $objWorkSheet->mergeCells('A27:H27');
    $objWorkSheet->mergeCells('A43:H43');
    $objWorkSheet->mergeCells('A59:H59');

    $objWorkSheet->getRowDimension('4')->setRowHeight(30);
    $objWorkSheet->setCellValue('A2','កាលបរិច្ចេត​ទាញយក : '.$current_date);
    $objWorkSheet->getStyle("A2:H2")->applyFromArray($styledate);
    $objWorkSheet->getStyle("A5:A27")->applyFromArray($styledate);
    $objWorkSheet->getStyle("A4:H4")->applyFromArray($styletitle);
    $objWorkSheet->getStyle("A12:H12")->applyFromArray($styletitle);
    $objWorkSheet->getStyle("B5:B59")->applyFromArray($stylecontent);
    $objWorkSheet->getStyle('A27:H27')->applyFromArray($styledate);
    $objWorkSheet->getColumnDimension("A")->setAutoSize(true);

    $rowCount = 4;
    $customTitle_kh = array ('ឈ្មោះ​គណនីជាខ្មែរ','ឈ្មោះ​គណនីជាអក្សរឡាតាំង','លេខគណនី','ប្រភេទគណនី','ប្រភេទរូបិយប័ណ្ណ','សេវកម្ម','និវាសនដ្ឋាន');
    $customTitle_en = array ('Account Name in Khmer','Account Name in Latin','Account Number','Account Type','Currency Type','Services','Resident Status');
    $colName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O');

    $personalinfo_kh = array ('លេខ​ CIF','ឈ្មោះអតិថិជន','អក្សរ​ឡាតាំង','ថ្ងៃខែឆ្នាំកំណើត','សញ្ជាតិ','ភេទ','អត្តសញ្ញាណអតិថិជន','លេខអត្តសញ្ញាណ​','មុខរបរ','នៅគ្រឹះស្ថាន','លេខទូរស័ព្ទ','អ៊ីម៉ែល​​','អសយដ្ឋានបច្ចុប្បន្ន');
    $personalinfo_en = array ('CIF Nº','Name','Name in Latin','Date of Birth','Nationality','Gender','Type of ID','ID Number','Occupuation','Institution','Phone Number','E-mail','Correspondent Address');
    $typeofid = array(1=>'អត្តសញ្ញាណ្ណប័ណ្ណ / National ID Card',2=>'លិខិតឆ្លងដែន​ / Passport',3=>'ប័ណ្ណបើកបរ / Driver License',4=>'ផ្សេងៗ / Other');



    $objWorkSheet->SetCellValue('A4',"I.ព័ត៌មានគណនី/ ACCOUNT INFORMATION");
    $objWorkSheet->SetCellValue('A5',$customTitle_kh[0].' / '.$customTitle_en[0]);
    $objWorkSheet->SetCellValue('A6',$customTitle_kh[1].' / '.$customTitle_en[1]);
    $objWorkSheet->SetCellValue('A7',$customTitle_kh[2].' / '.$customTitle_en[2]);
    $objWorkSheet->SetCellValue('A8',$customTitle_kh[3].' / '.$customTitle_en[3]);
    $objWorkSheet->SetCellValue('A9',$customTitle_kh[4].' / '.$customTitle_en[4]);
    $objWorkSheet->SetCellValue('A10',$customTitle_kh[5].' / '.$customTitle_en[5]);
    $objWorkSheet->SetCellValue('A11',$customTitle_kh[6].' / '.$customTitle_en[6]);

    $objWorkSheet->SetCellValue('A12',"II.ព័ត៌មានផ្ទាល់ខ្លួន/ PERSONAL INFORMATION");

    $objWorkSheet->SetCellValue('A13','1. '.$personalinfo_kh[0].' / '.$personalinfo_en[0]);
    $objWorkSheet->SetCellValue('A14',$personalinfo_kh[1].' / '.$personalinfo_en[1]);
    $objWorkSheet->SetCellValue('A15',$personalinfo_kh[2].' / '.$personalinfo_en[2]);
    $objWorkSheet->SetCellValue('A16',$personalinfo_kh[3].' / '.$personalinfo_en[3]);
    $objWorkSheet->SetCellValue('A17',$personalinfo_kh[4].' / '.$personalinfo_en[4]);
    $objWorkSheet->SetCellValue('A18',$personalinfo_kh[5].' / '.$personalinfo_en[5]);
    $objWorkSheet->SetCellValue('A19',$personalinfo_kh[6].' / '.$personalinfo_en[6]);
    $objWorkSheet->SetCellValue('A20',$personalinfo_kh[7].' / '.$personalinfo_en[7]);
    $objWorkSheet->SetCellValue('A21',$personalinfo_kh[8].' / '.$personalinfo_en[8]);
    $objWorkSheet->SetCellValue('A22',$personalinfo_kh[9].' / '.$personalinfo_en[9]);
    $objWorkSheet->SetCellValue('A23',$personalinfo_kh[10].' / '.$personalinfo_en[10]);
    $objWorkSheet->SetCellValue('A24',$personalinfo_kh[11].' / '.$personalinfo_en[11]);
    $objWorkSheet->SetCellValue('A25',$personalinfo_kh[12].' / '.$personalinfo_en[12]);
    $objWorkSheet->SetCellValue('A26','តើលោកអ្នក​ជាបុគ្គល​អាមេរិកដែរឬទេ?​ / Are you US person?');
    $objWorkSheet->SetCellValue('A27','សម្គាល់៖ ប្រសិនបើ បាទ/ចា សូមបំពេញ​ទម្រង់​ W9 / Note: If yes, please fill the W9 form.');


    $objWorkSheet->getColumnDimension('A')->setWidth(30);
    $objWorkSheet->getColumnDimension('B')->setWidth(10);
    $objWorkSheet->getColumnDimension('C')->setWidth(10);
    $objWorkSheet->getColumnDimension('D')->setWidth(10);
    $objWorkSheet->getColumnDimension('E')->setWidth(10);
    $objWorkSheet->getColumnDimension('F')->setWidth(10);

    $id = $row['id'];
    $customer_name = $row['account_name_kh'];
    $customer_name_latin = $row['account_name_latin'];
    $account_number = $row['account_number'];
    $account_type = $row['account_type'];
    $currency_type = $row['currency_type'];
    $services = explode(',',$row['services']);

    if(count($services)){
        $services_selected = isset($services[0])?$services_list[$services[0]]:'';
        $services_selected .= isset($services[1])?'  |  '.$services_list[$services[1]]:'';
    }
    $resident_status = $row['resident_status'];
    $branch_name = trim($row['branch_name']);
    $request_date = $row['created_date'];

    $renationship = $row['relationship'];
    $re_name = $row['name'];
    $name_latin = $row['name_latin'];
    $dob = $row['dob'];
    $nationality = $row['nationality'];
    $gender = $row['gender']=='Male'?'ប្រុស / Male':'ស្រី / Female';
    $type_of_id = $row['type_of_id'];
    $id_number = $row['id_number'];
    $id_issured_date = $row['id_issured_date'];
    $id_issured_date = $row['id_issured_date'];
    $occupation = $row['occupation'];
    $institution = $row['institution'];
    $phone_number = $row['phone_number'];
    $email_address = $row['email_address'];
    $address = $row['address'];
    $is_us = $row['is_us']?'បាទ / Yes':'ទេ / ​No';

    $objWorkSheet->SetCellValue('B5',$customer_name);
    $objWorkSheet->SetCellValue('B6',$customer_name_latin);
    $objWorkSheet->SetCellValue('B7',$account_number);
    $objWorkSheet->SetCellValue('B8',$account_type_list[$account_type]);
    $objWorkSheet->SetCellValue('B9',$currency_type_list[$currency_type]);
    $objWorkSheet->SetCellValue('B10',$services_selected);
    $objWorkSheet->SetCellValue('B11',$resient_status_list[$resident_status]);

    
    $objWorkSheet->SetCellValue('B14',$re_name);
    $objWorkSheet->SetCellValue('B15',$name_latin);
    $objWorkSheet->SetCellValue('B16',$dob);
    $objWorkSheet->SetCellValue('B17',$nationality);
    $objWorkSheet->SetCellValue('B18',$gender);
    $objWorkSheet->SetCellValue('B19',$typeofid[$type_of_id]);
    $objWorkSheet->SetCellValue('B20',$id_number);
    $objWorkSheet->SetCellValue('B21',$occupation);
    $objWorkSheet->SetCellValue('B22',$institution);
    $objWorkSheet->SetCellValue('B23',$phone_number);
    $objWorkSheet->SetCellValue('B24',$email_address);
    $objWorkSheet->SetCellValue('B25',$address);
    $objWorkSheet->SetCellValue('B26',$is_us);

    $sub_query = "SELECT 
       p.relationship,p.name,p.name_latin,p.dob,p.nationality,p.gender,p.type_of_id ,p.id_number,p.is_us,
        p.id_issured_date,p.id_expiry_date,p.occupation,p.institution,p.phone_number,p.email_address,p.address
        FROM deposit_customer_personal_info as p
        where p.customer_id = $id and p.relationship IS NOT NULL
        ORDER BY p.created_date DESC ";
        // echo $sub_query;
        // exit;
         $sub_results = $conn->query($sub_query);
        if($sub_results->num_rows){
            $j = 2;
            $colrow = 28;
            $col = 28;
            $mergerow = 43;
            while($rows = mysqli_fetch_assoc($sub_results)){
                //Data handler
                $relationship = $rows['relationship'];
                $re_name = $rows['name'];
                $re_name_latin = $rows['name_latin'];
                $re_dob = $rows['dob'];
                $re_nationality = $rows['nationality'];
                $re_gender = $rows['gender']=="Male"?'ប្រុស / Male':'ស្រី / Female';
                $re_type_of_id = $rows['type_of_id'];
                $re_id_number = $rows['id_number'];
                $re_id_issured_date = $rows['id_issured_date'];
                $re_id_expiry_date = $rows['id_expiry_date'];
                $re_occupation = $rows['occupation'];
                $re_institution = $rows['institution'];
                $re_phone_number= $rows['phone_number'];
                $re_email_address= $rows['email_address'];
                $re_address= $rows['address'];
                $re_is_us = $row['is_us']?'បាទ / Yes':'ទេ / ​No';


                $objWorkSheet->getStyle('A'.$col.':A'.($col+15))->applyFromArray($styledate);
                $objWorkSheet->getStyle('A'.$mergerow.':H'.$mergerow)->applyFromArray($styledate);

                $objWorkSheet->mergeCells('B'.$colrow.':H'.$colrow);
                $objWorkSheet->mergeCells('B'.($colrow + 1).':H'.($colrow + 1));
                $objWorkSheet->mergeCells('B'.($colrow + 2).':H'.($colrow + 2));
                $objWorkSheet->mergeCells('B'.($colrow + 3).':H'.($colrow + 3));
                $objWorkSheet->mergeCells('B'.($colrow + 4).':H'.($colrow + 4));
                $objWorkSheet->mergeCells('B'.($colrow + 5).':H'.($colrow + 5));
                $objWorkSheet->mergeCells('B'.($colrow + 6).':H'.($colrow + 6));
                $objWorkSheet->mergeCells('B'.($colrow + 7).':H'.($colrow + 7));
                $objWorkSheet->mergeCells('B'.($colrow + 8).':H'.($colrow + 8));
                $objWorkSheet->mergeCells('B'.($colrow + 9).':H'.($colrow + 9));
                $objWorkSheet->mergeCells('B'.($colrow + 10).':H'.($colrow + 10));
                $objWorkSheet->mergeCells('B'.($colrow + 11).':H'.($colrow + 11));
                $objWorkSheet->mergeCells('B'.($colrow + 12).':H'.($colrow + 12));
                $objWorkSheet->mergeCells('B'.($colrow + 13).':H'.($colrow + 13));
                $objWorkSheet->mergeCells('B'.($colrow + 14).':H'.($colrow + 14));
               


                $objWorkSheet->SetCellValue('A'.$colrow,$j.'.  សម្ព័ន្ធភាព / Relationship');
                $objWorkSheet->SetCellValue('A'.($colrow + 1),$personalinfo_kh[0].' / '.$personalinfo_en[0]);
                $objWorkSheet->SetCellValue('A'.($colrow + 2),$personalinfo_kh[1].' / '.$personalinfo_en[1]);
                $objWorkSheet->SetCellValue('A'.($colrow + 3),$personalinfo_kh[2].' / '.$personalinfo_en[2]);
                $objWorkSheet->SetCellValue('A'.($colrow + 4),$personalinfo_kh[3].' / '.$personalinfo_en[3]);
                $objWorkSheet->SetCellValue('A'.($colrow + 5),$personalinfo_kh[4].' / '.$personalinfo_en[4]);
                $objWorkSheet->SetCellValue('A'.($colrow + 6),$personalinfo_kh[5].' / '.$personalinfo_en[5]);
                $objWorkSheet->SetCellValue('A'.($colrow + 7),$personalinfo_kh[6].' / '.$personalinfo_en[6]);
                $objWorkSheet->SetCellValue('A'.($colrow + 8),$personalinfo_kh[7].' / '.$personalinfo_en[7]);
                $objWorkSheet->SetCellValue('A'.($colrow + 9),$personalinfo_kh[8].' / '.$personalinfo_en[8]);
                $objWorkSheet->SetCellValue('A'.($colrow + 10),$personalinfo_kh[9].' / '.$personalinfo_en[9]);
                $objWorkSheet->SetCellValue('A'.($colrow + 11),$personalinfo_kh[10].' / '.$personalinfo_en[10]);
                $objWorkSheet->SetCellValue('A'.($colrow + 12),$personalinfo_kh[11].' / '.$personalinfo_en[11]);
                $objWorkSheet->SetCellValue('A'.($colrow + 13),$personalinfo_kh[12].' / '.$personalinfo_en[12]);
                $objWorkSheet->SetCellValue('A'.($colrow + 14),'តើលោកអ្នក​ជាបុគ្គល​អាមេរិកដែរឬទេ?​ / Are you US person?');
                $objWorkSheet->SetCellValue('A'.($colrow + 15),'សម្គាល់៖ ប្រសិនបើ បាទ/ចា សូមបំពេញ​ទម្រង់​ W9 / Note: If yes, please fill the W9 form.');

                //Set Value
                $objWorkSheet->SetCellValue('B'.$colrow,$relationship);
                $objWorkSheet->SetCellValue('B'.($colrow + 1),'');
                $objWorkSheet->SetCellValue('B'.($colrow + 2), $re_name);
                $objWorkSheet->SetCellValue('B'.($colrow + 3),$re_name_latin);
                $objWorkSheet->SetCellValue('B'.($colrow + 4), $re_dob);
                $objWorkSheet->SetCellValue('B'.($colrow + 5), $re_nationality);
                $objWorkSheet->SetCellValue('B'.($colrow + 6), $re_gender);
                $objWorkSheet->SetCellValue('B'.($colrow + 7),$typeofid[$re_type_of_id]);
                $objWorkSheet->SetCellValue('B'.($colrow + 8), $re_id_number);
                $objWorkSheet->SetCellValue('B'.($colrow + 9), $re_occupation);
                $objWorkSheet->SetCellValue('B'.($colrow + 10),$re_institution);
                $objWorkSheet->SetCellValue('B'.($colrow + 11), $re_phone_number);
                $objWorkSheet->SetCellValue('B'.($colrow + 12),$re_email_address);
                $objWorkSheet->SetCellValue('B'.($colrow + 13), $re_address);
                $objWorkSheet->SetCellValue('B'.($colrow + 14), $re_is_us);
                $j++;
                $col = $col+15;
                $colrow=$colrow+16;
                $mergerow = $mergerow+16;
                
            }
        }

    $objWorkSheet->setTitle("$customer_name");
    $n++;
    $i++;
  }
}

$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
ob_end_clean();
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="Report_deposit.xlsx"');
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
$message = '<span style="color:#4DB848;">Data have been successful exported.</span>';
// exit;
?>