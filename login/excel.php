<?php 
error_reporting(E_ALL);
 $items_checked = isset($_POST['checkboxitem'])?$_POST['checkboxitem']:'';
 if($items_checked){
   $items_checked = implode(',',$items_checked);
      include('../library/PHPExcel/Classes/PHPExcel.php');
      $objPHPExcel = new PHPExcel();

      $date = new DateTime();
      $date->setTimezone(new DateTimeZone('Asia/Phnom_Penh'));
      $current_date = $date->format("F d Y H:i:s");
    
      if($service_type=='deposit'){
        include('excel-deposit.php');
      }else{
        include('excel-loan.php');
      }
      $message = '<span style="color:#4DB848;">Data have been successful exported.</span>';
      exit;
  }else{
    $message = '<span style="color:#ee6e73;">No item selected to export.</span>';
    exit;
  }
?>