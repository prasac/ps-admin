<?php
   $query = "SELECT
    d.id,d.account_name_kh,d.account_name_latin,d.account_number,d.account_type,d.currency_type,d.services,d.resident_status,d.branch,d.is_view,d.created_date
    ,dp.cif_number,dp.name,dp.name_latin,dp.dob,dp.nationality,dp.gender,dp.type_of_id,dp.id_number,dp.id_issured_date,dp.id_expiry_date,
    dp.occupation,dp.institution,dp.phone_number,dp.email_address,dp.address,dp.is_us,td.date,td.applicant_name,td.account_name,td.id_number,td.relate_account_number,
    td.td_account_number,td.account_type,td.currency_type,td.deposit_amount,td.deposit_amount_in_word,td.deposit_date,td.term_of_deposit,td.td_condition,
    td.roll_over_type,td.roll_over_sa_amount,td.created_date
    FROM deposit_customer_info as d
    inner join deposit_customer_personal_info as dp on d.id=dp.customer_id
    left join term_deposit as td on d.id=td.customer_id
    left join branches as b on d.duty_station=b.id
    where d.id in($items_checked) and dp.relationship IS NULL
    ORDER BY d.account_name_kh ASC";
    $result = $conn->query($query);
    $idtype = array(1=>'National ID Card',2=>'Passport',3=>'Driver License',4=>'Others');
    $n=0;
    while($row = mysqli_fetch_assoc($result)) {
      $n++;
      if($n > 1){
        $mpdf->AddPage('P', // L - landscape, P - portrait 
            '', '', '',
            8,    // 15 margin_left
            8,    // 15 margin right
            5,     // 16 margin top
            5,    // margin bottom
            5,     // 9 margin header
            5     // 9 margin footer
            ); // margin footer
      }
      $id = $row['id'];
      //======Account Info====
      $account_name_kh = $row['account_name_kh'];
      $account_name_latin = $row['account_name_latin'];
      $account_number = $row['account_number'];
      $account_type = $row['account_type'];
      $currency_type = $row['currency_type'];
      $services = $row['services']?explode(',',$row['services']):0;
      $resident_status = $row['resident_status']?$row['resident_status']:0;
      $branch = $row['branch'];
      
      $cif_number = $row['cif_number'];
      $name = $row['name'];
      $name_latin = $row['name_latin'];
      $dob = $row['dob'];
      $nationality = $row['nationality'];
      $gender = $row['gender'];
      $type_of_id = $row['type_of_id'];
      $id_number = $row['id_number'];
      $id_issured_date = $row['id_issured_date'];
      $id_expiry_date = $row['id_expiry_date'];
      $occupation = $row['occupation'];
      $institution = $row['institution'];
      $phone_number = $row['phone_number'];
      $email_address = $row['email_address'];
      $address = $row['address'];
      $is_us = $row['is_us'];

      //======Td=======
      $td_date = $row['date']?$row['date']:'';
      $td_account_name = $row['account_name']?$row['account_name']:'';
      $td_applicant_name = $row['applicant_name']?$row['applicant_name']:'';
      $td_id_number = $row['id_number']?$row['id_number']:'';
      $td_relate_account_number = $row['relate_account_number']?$row['relate_account_number']:'';
      $td_account_number = $row['td_account_number']?$row['td_account_number']:'';
      $td_account_type = $row['account_type']?$row['account_type']:'';
      $td_currency_type = $row['currency_type']?$row['currency_type']:'';
      $td_deposit_amount = $row['deposit_amount']?$row['deposit_amount']:'';
      $td_deposit_amount_in_word = $row['deposit_amount_in_word']?$row['deposit_amount_in_word']:'';
      $td_deposit_date = $row['deposit_date']?$row['deposit_date']:'';
      $td_term_of_deposit = $row['term_of_deposit']?$row['term_of_deposit']:'';
      $td_td_condition = $row['td_condition']?$row['td_condition']:'';
      $td_roll_over_type = $row['roll_over_type']?$row['roll_over_type']:'';
      $td_roll_over_sa_amount = $row['roll_over_sa_amount']?$row['roll_over_sa_amount']:'';

     
      $mpdf->SetHTMLFooter('<div class="pdf-footer"><div class="footer-right">SA 001.V3​</div><div class="footer-left">Form : Personal Account Opening Application | Private Company </div></div>');
      $html = '
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <body>
          <head>
          </head>
          <body>
          <style>
          html,body{
            font-family:hanuman,Verdana;
            font-size:14px;
          }
          h1.khmer{
            font-family:"khmerosmoul",Arail;
            font-weight:lighter;
            font-size:1.1em;
            margin:0;
            padding:0;
          }
          h1.khmer.right{
            font-family:"khmerosmoul",Arail;
            font-weight:lighter;
            font-size:4em;
            margin:0;
            padding:0;
          }
          strong.khmer{
            font-family:"khmerosmoul";
            font-weight:lighter;
            font-size:1.1em;
            line-height:1.8em;
          }
          h1.khmer.center{
            font-family:"khmerosmoul";
            font-weight:lighter;
            font-size:1.1em;
            text-align:center;
            line-height:1.8em;
          }

          h1.underline{
            text-decoration:underline;
          }
          div.right{
            text-align:right;
            float:right;
            margin-top:-2.5em;
          }
          .right{
            text-align:right;
            float:right;

          }
          .center,strong.center,center.center{
            text-align:center;
          }
          p{
            line-height:1.8em;
            font-size:14px;
          }
          p.khmer,span.khmer{
            font-family:"hanuman";
            font-size:14px;
            text-lign:justify;
          }
          table{
            border-collapse:collapse;
          }
          table thead tr th{
              valign:middle;
              padding:5px;
              font-weight:bold;
          }
          table tbody tr td{
            valign:middle;
            padding:5px;
          }
          table tbody tr td.center{
            text-align:center;
          }
          .pdf-footer{
            font-size:10px;
            padding:4px 0;
          }
          .footer-left{
            float:left;
            text-align:left;
            width:40%;
          }
          .footer-right{
            float:right;
            text-align:right;
            font-family:"hanuman";
            width:40%;
          }
          p.khmer.content{
            line-height:2;
            text-align:justify !important;
            width:100%;
          }
          input.khmer{
            font-family:"khmeros";
          }

          div.head-wraper{
            width:100%;
            margin:0 auto;
            overflow:hidden;
          }
          div.content{
            width:100%;
            overflow:hidden;
          }
          div.body-pdf{
            width:100%;
            margin:0 auto;
          }
       

          div.content div.label{
            width:20%;
            float:left;
          }
          div.content div.data{
            width:80%;
            float:right;
          }
          div.sub-label{
            width:22%;
            text-align:left;
            float:left;
            font-size:11px;
            line-height:0;
            margin-top:-5px;
          }
          div.sub-label-full{
            width:100%;
            text-align:left;
            float:left;
            font-size:11px;
            line-height:0;
            margin-top:-5px;
            border:0px solid black;
          }
          div.sub-label-right{
            width:77%;
            text-align:left;
            float:right;
            font-size:11px;
            margin-top:-5px;
            border:0px solid red;
          }
          div.sub-label-right span.item{
            display:block; 
            border:0px solid blue;
            margin:0 20px;
          }
          div.content-second{
            border:1px solid #000000;
            padding:4px 5px;
            margin:5px auto;
          }
          div.content-second div.label{
            width:21%;
            float:left;
          }
          div.content-second div.data{
            width:79%;
          }
          div.content-second .content{
            margin:0 auto;
            padding:0 15px;
            width:100%;
          }
          div.content-second .content div.label{
            width:30%;
            float:left;
          }
          div.content-second .content div.data{
            width:70%;
            float:right;
          }
          div.content-second .content div.left{
            width:50%;
            display:block;
          }
          div.content-second .content div.right{
            width:50%;
            text-align:left;
            display:inline-block;
            float:right;
          }
          div.content-second .content div.sub-label{
            width:80%;
            text-align:left;
            float:left;
            font-size:11px;
            line-height:0;
          }
          div.content-second div.left div.label{
            width:37%;
            float:left;
          }
          div.content-second .left div.data{
            width:63%;
            float:right;
          }

          div.content-second .content div.label-full{
            width:19%;
            float:left;
          }
          div.content-second .content div.data-full{
            width:81%;
            float:right;
            margin-top:-2px;
          }
          div.content-second .content div.data-full-width{
            width:100%;
            margin:0 auto;
          }
          div.content-second .content div.sub-label-full-width{
            width:100%;
            margin:0 auto;
            font-size:11px;
          }
          
          div.content-final{
            width:100%;
            margin:0 auto;
          }
          div.content-second .content .right div.sub-label-full{
            width:100%;
            border:0px solid blue;
          }
          div.content div.left,div.content div.right{
            width:50%;
          }

          div.content div.left div.label,
          div.content div.right div.label{
            width:40%;
            text-align:left;
          }
          div.content div.left div.data,div.content div.right div.data{
            width:60%;
            text-align:left;
            font-weight:bold;
          }

          div.content div.left div.sub-label,div.content div.right div.sub-label {
            width:100%;
            text-align:left;
            font-size:11px;
            margin-top:-5px;
          }
          div.account_hold_wrapper{
            width:100%;
            margin:0 auto;
          }
          div.account_hold{
            width:25%;
            border-top:1px solid #000000;
            padding:10px;
            margin:0 24px;
            float:left;
          }
          div.account_hold p{text-align:center;}
          div.prasac_use_blog_wrapper{
            width:100%;
            margin:0 auto;
            margin-top:1.5em;
            border:1px solid #000000;
            overflow:hidden;
          }
          div.prasac_use_blog_wrapper div.prasac_use_blog{
            width:30.8%;
            float:left;
            padding:0 8px;
            text-align:center;
          }
          div.prasac_use_blog_wrapper div.prasac_use_blog p{
            font-size:12px;
            border-top:1px solid #000000;
            padding:0;
            margin-top:8.2em;
          }
          </style>
            <form>
              <div class="head-wraper">
                <div  style="border:0px solid red;float:left;width:50%;padding:0;">
                    <img src="../images/logo-website.png" alt="" title="" class="img-responsive" width="90">
                </div>
                <div style="border:0px solid red;float:right;width:50%;padding:0;font-size:1.3em;">
                  <h1 class="khmer right">ពាក្យស្នើសុំបើកគណនី​​</h1>
                  <h1 class="khmer right">ACCOUNT OPENING APPLICATION</h1>
                </div>
              </div>
              <div class="body-pdf">
                <h1 class="khmer">I.ព័ត័មានគណនី ​​/ ACCOUNT INFORMATION</h1>
                <div class="content">
                  <div class="left">
                    <div class="label">ឈ្មោះគណនីជាខ្មែរ</div>
                    <div class="data">: '.$account_name_kh.'</div>
                    <div class="sub-label">Account Name in Khmer</div>
                  </div>
                  <div class="right">
                    <div class="label">ឈ្មោះគណនីជាអក្សរឡាតាំង </div>
                    <div class="data">: '.$account_name_latin.'</div>
                    <div class="sub-label">Account Name in Latin</div>
                  </div>
                </div>
                <div class="content">
                  <div class="label">លេខគណនី</div>
                  <div class="data">: '.$account_number.'</div>
                  <div class="sub-label">Account Number</div>
                </div>
                <div class="content">
                  <div class="label">ប្រភេទគណនី</div>
                  <div class="data">: ';
                  $savings = $account_type==1?'checked="checked"':'';
                  $unfixed = $account_type==2?'checked="checked"':'';
                  $retirement = $account_type==3?'checked="checked"':'';
                  $other = $account_type==4?'checked="checked"':'';

                  $currency_khr = $currency_type==1?'checked="checked"':'';
                  $currency_usd = $currency_type==2?'checked="checked"':'';
                  $currency_thb = $currency_type==3?'checked="checked"':'';
                  $currency_other = $currency_type==4?'checked="checked"':'';

                  $service_atm = in_array(1,$services)?'checked="checked"':'';
                  $service_mib = in_array(2,$services)?'checked="checked"':'';
                  $resident = $resident_status?'checked="checked"':'';
                  $non_resident = !$resident_status?'checked="checked"':'';
                  $html .='
                    <input type="checkbox" '.$savings.'  name="saving" value="saving"/> គណនីសន្សំ 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" '.$unfixed.' name="unfixed"  value="unfixed"/> គណនីចម្រើនគ្មានកាលកំណត់ 
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" '.$retirement.' name="retired" value="retired"/> គណនីសោធននិវត្តន៌
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" '.$other.' name="other" value="other"/> ផ្សេងៗ...............
                  </div>
                  <div class="sub-label">Accout Type</div>
                  <div class="sub-label-right">
                    &nbsp;
                    Savings Account &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    Unfixed Deposit Account &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    Retirement Account &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Others
                  </div>
                </div>
                <div class="content">
                  <div class="label">ប្រភេទរូបិយប័ណ្ឌ</div>
                  <div class="data">: 
                    <input type="checkbox" '.$currency_khr.' name="khr" value="khr"/> ប្រាក់រៀល 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" '.$currency_usd.' name="usd" value="usd"/> ប្រាក់ដុល្លាអាមេរិក 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" '.$currency_thb.' name="thb" value="thb"/> ប្រាក់បាត
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" '.$currency_other.' name="currency_other" value="other" /> ផ្សេងៗ...............
                  </div>
                  <div class="sub-label">Currency Type</div>
                  <div class="sub-label-right">
                    &nbsp;
                    KHR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    USD &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    THB &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Others
                  </div>
                </div>
                <div class="content">
                  <div class="label">សេវាកម្ម</div>
                  <div class="data">: 
                    <input type="checkbox" checked="checked" '.$service_atm.' name="atm" value="atm"/> ប័ណ្ណអេធីអឹម 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" '.$service_mib.' name="mib" value="mib"/> សេវាធនាគារចល័ត
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" name="service_other" value="other" /> ផ្សេងៗ..................
                  </div>
                  <div class="sub-label">Services</div>
                  <div class="sub-label-right">
                    &nbsp;
                    ATM  Card &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    Mobile and Internet Banking &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    Others
                  </div>
                </div>
                <div class="content">
                  <div class="label">និវាសនដ្ឋាន</div>
                  <div class="data">: 
                    <input type="checkbox" '.$resident.' name="resident" value="resident"/> និវាសនជន
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" '.$non_resident.' name="non-resident" value="non-resident"/> អនិវាសនជន
                  </div>
                  <div class="sub-label">Resident Status</div>
                  <div class="sub-label-right">
                    &nbsp;
                    Resident &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                    Non-Resident
                  </div>
                </div>
                <h1 class="khmer">II.ព័ត័មានផ្ទាល់ខ្លួន ​​/ PERSONAL INFORMATION</h1>
                <div class="content-second">
                  <div class="label">១.លេខ CIF / CIF No</div>
                  <div class="data">: ...............................</div>
                  <div class="content">
                    <div class="left">
                      <div class="label">ឈ្មោះ​អតិថិជន</div>
                      <div class="data">: '.$name.'</div>
                      <div class="sub-label">Name</div>
                    </div>
                    <div class="right">
                      <div class="label">អក្សរឡាតាំង</div>
                      <div class="data">: '.$name_latin.'</div>
                      <div class="sub-label">Latin</div>
                    </div>

                    <div class="left">
                      <div class="label">ថ្ងៃខែឆ្នាំកំណើត</div>
                      <div class="data">: '.date("d.m.Y",strtotime($dob)).'</div>
                      <div class="sub-label">Date of Birth</div>
                    </div>
                    <div class="right">
                      <div class="label">សញ្ជាតិ</div>
                      <div class="data">: '.$nationality.'
                      &nbsp;
                      ភេទ: ';
                      $gender_male = $gender=='Male'?'checked="checked"':'';
                      $gender_female = $gender=='Female'?'checked="checked"':'';
                      $html .='<input type="checkbox" '.$gender_male.' value="Male" name="gender"/> ប្រុស
                        <input type="checkbox" '.$gender_female.' value="Female" name="gender"/> ស្រី
                      </div>
                      <div class="sub-label">
                        Nationality 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        Gender &nbsp;&nbsp;&nbsp;
                        Male  &nbsp;&nbsp;&nbsp;&nbsp;
                        Female
                      </div>
                    </div><p></p>';
                    $id_card = $type_of_id==1?'checked="checked"':'';
                    $passport = $type_of_id==2?'checked="checked"':'';
                    $html.='
                    <div class="label-full">អត្តសញ្ញាណអតិថិជន</div>
                    <div class="data-full">: 
                      <input type="checkbox" '.$id_card.'  name="id_type1" value="idcard"/> អត្តសញ្ញាណប័ណ្ណ
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="checkbox" '.$passpor.' name="id_type1" value="passport"/> លិខិតឆ្លងដែន 
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="checkbox"  name="id_type1" value="other"/> ផ្សេងៗ.................. 
                    </div>
                    <div class="sub-label"  style="width:20%;">Type of ID</div>
                    <div class="sub-label-right" >
                      National ID Card &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                      Passport &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Others
                    </div>

                    <div class="data-full-width">
                      <div style="width:40%;float:left;">
                        លេខអត្តសញ្ញាណ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '.$id_number.'
                      </div>
                      <div style="width:30%;float:left;">
                        ថ្ងៃចេញផ្សាយ: '.date("d.m.Y",strtotime($id_issured_date)).'
                      </div>
                      <div style="width:29,5%;float:left;">
                        ថ្ងៃផុតកំណត់: '.date("d.m.Y",strtotime($id_expiry_date)).'
                      </div>
                    </div>
                    <div class="sub-label-full-width">
                      ID Number
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Issued Date
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;
                      Expiry Date
                    </div>
                    
                    <div class="left">
                      <div class="label">មុខរបរ</div>
                      <div class="data">: '.$occupation.'</div>
                      <div class="sub-label">Occupation</div>
                    </div>
                    <div class="right">
                      <div class="label">នៅស្ថាប័ន</div>
                      <div class="data">: '.$institution.'</div>
                      <div class="sub-label">Institution</div>
                    </div>
                    <div class="left">
                      <div class="label">លេខទូរស័ព្ទ</div>
                      <div class="data">: '.$phone_number.'</div>
                      <div class="sub-label">Phone Number</div>
                    </div>
                    <div class="right">
                      <div class="label">អុីម៉ែល</div>
                      <div class="data">: '.$email_address.'</div>
                      <div class="sub-label">E-mail</div>
                    </div><p></p>

                    <div class="label-full">អាសយដ្ឋានបច្ចុប្បន្ន</div>
                    <div class="data-full">:  '.$address.'</div>
                    <div class="sub-label-full">Correspondent Address</div>';
                    $is_us_chekced = $is_us?'checked="checked"':'';
                    $is_not_us_chekced = !$is_us?'checked="checked"':'';
                    $html.='
                    <div class="content-final">
                      តើលោកអ្នកជាបុគ្គលអាមេរិកដែរឬទេ?/ <span style="font-size:11px;">Are you US person?</span>
                      &nbsp;&nbsp;
                      <input type="checkbox" '.$is_us_chekced.' name="is_us1" value="1" /> បាទ/ចាស/Yes 
                      &nbsp;&nbsp;&nbsp;
                      <input type="checkbox" '.$is_not_us_chekced.' name="is_us1" value="0" /> ទេ/No
                    </div>
                    <div class="content-final sub-label-full"><strong>សម្គាល់៖</strong> ប្រសិនបើ <strong>បាទ/ចាស</strong> សូមបំពេញ​ទម្រង់ W9 / Note: If Yes, please fill th W9 form.</div>

                  </div>
                </div>'; 
                 //=====Personal Info======
                  $query_sub = "SELECT
                  dp.id,dp.customer_id,dp.relationship,dp.cif_number,dp.name,dp.name_latin,dp.dob,dp.nationality,dp.gender,dp.type_of_id,dp.id_number,
                  dp.id_issured_date,dp.id_expiry_date,dp.occupation,dp.institution,dp.phone_number,dp.email_address,dp.address,dp.is_us,dp.created_date
                  FROM  deposit_customer_personal_info as dp
                  where dp.customer_id = $id and dp.relationship IS NOT NULL
                  ORDER BY dp.created_date ASC";
                  $result_sub = $conn->query($query_sub);
                  if($result_sub->num_rows){
                    $row_num =2;
                    while($row = mysqli_fetch_assoc($result_sub)) {
                      $relationship = $row['relationship'];
                      $name_in_khmer = $row['name'];
                      $name_in_latin = $row['name_latin'];
                      $date_of_birth = $row['dob'];
                      $nationality = $row['nationality'];
                      $gender = $row['gender'];
                      $type_of_id = $row['type_of_id'];
                      $id_number = $row['id_number'];
                      $id_card_issued_date = $row['id_issured_date'];
                      $id_expiry_date = $row['id_expiry_date'];
                      $occupation = $row['occupation'];
                      $institution = $row['institution'];
                      $phone_number = $row['phone_number'];
                      $email_address = $row['email_address'];
                      $address = $row['address'];
                      $is_us = $row['is_us'];
                      $created_date = $row['created_date'];

                      $html .= '<div class="content-second">
                                  <div style="width:50%;float:right;">
                                    <div class="label"  style="width:38%;">លេខ CIF</div>
                                    <div class="data" style="width:62%;">:....................................</div>
                                    <div class="sub-label" style="width:100%;float:right;">CIF No</div>
                                  </div>
                                  <div style="width:50%;float:left;">
                                    <div class="label" style="width:42%;">'.$row_num.'​. សម្ព័ន្ធភាព</div>
                                    <div class="data" style="width:58%;">: '.$relationship.'</div>
                                    <div class="sub-label" style="width:95%;float:right;">Relationship</div>
                                  </div>
                                  <div class="content">
                                    <div class="left">
                                      <div class="label">ឈ្មោះ​អតិថិជន</div>
                                      <div class="data">: '.$name_in_khmer.'</div>
                                      <div class="sub-label">Name</div>
                                    </div>
                                    <div class="right">
                                      <div class="label">អក្សរឡាតាំង</div>
                                      <div class="data">: '.$name_in_latin.'</div>
                                      <div class="sub-label">Latin</div>
                                    </div>

                                    <div class="left">
                                      <div class="label">ថ្ងៃខែឆ្នាំកំណើត</div>
                                      <div class="data">: '.date("d.m.Y",strtotime($date_of_birth)).'</div>
                                      <div class="sub-label">Date of Birth</div>
                                    </div>
                                    <div class="right">
                                      <div class="label">សញ្ជាតិ</div>
                                      <div class="data">: '.$nationality.'
                                      &nbsp;
                                      ភេទ: ';
                                      $male_checked = $gender=='Male'?'checked="checked"':'';
                                      $female_checked = $gender=='Female'?'checked="checked"':'';
                                       $html .='
                                       <input type="checkbox" '.$male_checked.'  value="Male" name="gender'.$row_num.'"/> ប្រុស
                                        <input type="checkbox" '.$female_checked.' value="Female" name="gender'.$row_num.'"/> ស្រី
                                      </div>
                                      <div class="sub-label">
                                        Nationality 
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;
                                        Gender &nbsp;&nbsp;&nbsp;
                                        Male  &nbsp;&nbsp;&nbsp;&nbsp;
                                        Female
                                      </div>
                                    </div><p></p>';
                                    $id_card_selected = $type_of_id==1?'checked="checked"':'';
                                    $passport_selected = $type_of_id==2?'checked="checked"':'';
                                    $html .='
                                    <div class="label-full">អត្តសញ្ញាណអតិថិជន</div>
                                    <div class="data-full">: 
                                      <input type="checkbox"  '.$id_card_selected.' name="id_type'.$row_num.'" value="idcard"/> អត្តសញ្ញាណប័ណ្ណ
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <input type="checkbox" '.$passport_selected.' name="id_type'.$row_num.'" value="passport"/> លិខិតឆ្លងដែន 
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <input type="checkbox"  name="id_type'.$row_num.'" value="other"/> ផ្សេងៗ.................. 
                                    </div>
                                    <div class="sub-label"  style="width:20%;">Type of ID</div>
                                    <div class="sub-label-right" >
                                      National ID Card &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                      Passport &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      Others
                                    </div>

                                    <div class="data-full-width">
                                      <div style="width:40%;float:left;">
                                        លេខអត្តសញ្ញាណ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '.$id_number.'
                                      </div>
                                      <div style="width:30%;float:left;">
                                        ថ្ងៃចេញផ្សាយ: '.date("d.m.Y",strtotime($id_card_issued_date)).'
                                      </div>
                                      <div style="width:29.5%;float:left;">
                                        ថ្ងៃផុតកំណត់: '.date("d.m.Y",strtotime($id_expiry_date)).'
                                      </div>
                                    </div>
                                    <div class="sub-label-full-width">
                                        ID Number
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Issued Date
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;
                                        Expiry Date
                                    </div>
                                    
                                    <div class="left">
                                      <div class="label">មុខរបរ</div>
                                      <div class="data">: '.$occupation.'</div>
                                      <div class="sub-label">Occupation</div>
                                    </div>
                                    <div class="right">
                                      <div class="label">នៅស្ថាប័ន</div>
                                      <div class="data">: '.$institution.'</div>
                                      <div class="sub-label">Institution</div>
                                    </div>
                                    <div class="left">
                                      <div class="label">លេខទូរស័ព្ទ</div>
                                      <div class="data">: '.$phone_number.'</div>
                                      <div class="sub-label">Phone Number</div>
                                    </div>
                                    <div class="right">
                                      <div class="label">អុីម៉ែល</div>
                                      <div class="data">: '.$email_address.'</div>
                                      <div class="sub-label">E-mail</div>
                                    </div><p></p>

                                    <div class="label-full">អាសយដ្ឋានបច្ចុប្បន្ន</div>
                                    <div class="data-full">:  '.$address.'</div>
                                    <div class="sub-label-full">Correspondent Address</div>';
                                    $is_us_chekced = $is_us?'checked="checked"':'';
                                    $is_not_us_chekced = !$is_us?'checked="checked"':'';
                                    $html.='
                                    <div class="content-final">
                                      តើលោកអ្នកជាបុគ្គលអាមេរិកដែរឬទេ?/ <span style="font-size:11px;">Are you US person?</span>
                                      &nbsp;&nbsp;
                                      <input type="checkbox" '.$is_us_chekced.' name="is_us'.$row_num.'" value="1" /> បាទ/ចាស/Yes 
                                      &nbsp;&nbsp;&nbsp;
                                      <input type="checkbox" '.$is_not_us_chekced.' name="is_us'.$row_num.'" value="0" /> ទេ/No
                                    </div>
                                    <div class="content-final sub-label-full"><strong>សម្គាល់៖</strong> ប្រសិនបើ <strong>បាទ/ចាស</strong> សូមបំពេញ​ទម្រង់ W9 / Note: If Yes, please fill th W9 form.</div>
                                  </div>
                                </div>';
                                $row_num++;
                              }
                            }else{
                              $html .= '<div class="content-second">
                                  <div style="width:50%;float:right;">
                                    <div class="label"  style="width:38%;">លេខ CIF</div>
                                    <div class="data" style="width:62%;">:....................................</div>
                                    <div class="sub-label" style="width:100%;float:right;">CIF No</div>
                                  </div>
                                  <div style="width:50%;float:left;">
                                    <div class="label" style="width:42%;">២​. សម្ព័ន្ធភាព</div>
                                    <div class="data" style="width:58%;">: .....................</div>
                                    <div class="sub-label" style="width:95%;float:right;">Relationship</div>
                                  </div>
                                  <div class="content">
                                    <div class="left">
                                      <div class="label">ឈ្មោះ​អតិថិជន</div>
                                      <div class="data">: ...................</div>
                                      <div class="sub-label">Name</div>
                                    </div>
                                    <div class="right">
                                      <div class="label">អក្សរឡាតាំង</div>
                                      <div class="data">: .........................</div>
                                      <div class="sub-label">Latin</div>
                                    </div>

                                    <div class="left">
                                      <div class="label">ថ្ងៃខែឆ្នាំកំណើត</div>
                                      <div class="data">: ...........................</div>
                                      <div class="sub-label">Date of Birth</div>
                                    </div>
                                    <div class="right">
                                      <div class="label">សញ្ជាតិ</div>
                                      <div class="data">: ...............
                                      &nbsp;
                                      ភេទ: 
                                      <input type="checkbox"  value="Male" name="gender2"/> ប្រុស
                                        <input type="checkbox"  value="Female" name="gender2"/> ស្រី
                                      </div>
                                      <div class="sub-label">
                                        Nationality 
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;
                                        Gender &nbsp;&nbsp;&nbsp;
                                        Male  &nbsp;&nbsp;&nbsp;&nbsp;
                                        Female
                                      </div>
                                    </div><p></p>
                                    <div class="label-full">អត្តសញ្ញាណអតិថិជន</div>
                                    <div class="data-full">: 
                                      <input type="checkbox"  name="id_type2" value="idcard"/> អត្តសញ្ញាណប័ណ្ណ
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <input type="checkbox" name="id_type2" value="passport"/> លិខិតឆ្លងដែន 
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <input type="checkbox"  name="id_type2" value="other"/> ផ្សេងៗ.................. 
                                    </div>
                                    <div class="sub-label"  style="width:20%;">Type of ID</div>
                                    <div class="sub-label-right" >
                                      National ID Card &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                      Passport &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                      Others
                                    </div>

                                    <div class="data-full-width">
                                      <div style="width:40%;float:left;">
                                        លេខអត្តសញ្ញាណ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: .........
                                      </div>
                                      <div style="width:30%;float:left;">
                                        ថ្ងៃចេញផ្សាយ:....................
                                      </div>
                                      <div style="width:29.5%;float:left;">
                                        ថ្ងៃផុតកំណត់: .....................
                                      </div>
                                    </div>
                                    <div class="sub-label-full-width">
                                        ID Number
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Issued Date
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;
                                        Expiry Date
                                    </div>
                                    
                                    <div class="left">
                                      <div class="label">មុខរបរ</div>
                                      <div class="data">: ..................</div>
                                      <div class="sub-label">Occupation</div>
                                    </div>
                                    <div class="right">
                                      <div class="label">នៅស្ថាប័ន</div>
                                      <div class="data">: ....................</div>
                                      <div class="sub-label">Institution</div>
                                    </div>
                                    <div class="left">
                                      <div class="label">លេខទូរស័ព្ទ</div>
                                      <div class="data">: ......................</div>
                                      <div class="sub-label">Phone Number</div>
                                    </div>
                                    <div class="right">
                                      <div class="label">អុីម៉ែល</div>
                                      <div class="data">: ......................</div>
                                      <div class="sub-label">E-mail</div>
                                    </div><p></p>

                                    <div class="label-full">អាសយដ្ឋានបច្ចុប្បន្ន</div>
                                    <div class="data-full">:  ..................</div>
                                    <div class="sub-label-full">Correspondent Address</div>
                                    <div class="content-final">
                                      តើលោកអ្នកជាបុគ្គលអាមេរិកដែរឬទេ?/ <span style="font-size:11px;">Are you US person?</span>
                                      &nbsp;&nbsp;
                                      <input type="checkbox" name="is_us2" value="1" /> បាទ/ចាស/Yes 
                                      &nbsp;&nbsp;&nbsp;
                                      <input type="checkbox"  name="is_us2" value="0" /> ទេ/No
                                    </div>
                                    <div class="content-final sub-label-full"><strong>សម្គាល់៖</strong> ប្រសិនបើ <strong>បាទ/ចាស</strong> សូមបំពេញ​ទម្រង់ W9 / Note: If Yes, please fill th W9 form.</div>
                                  </div>
                                </div>';
                            }

                $html .='<h1 class="khmer">III.ការបញ្ជាក់និងអះអាង ​​/ DECLARATION</h1>
                <div class="content" style="padding:0 10px;">
                  <p>
                    ១. ខ្ញុំ​/យើង​ខ្ញុំ​ សូមអះអាង​ថា រាល់ព័ត៌មាន​ទាំង​អស់​ដែល​បាន​ផ្ដល់​ជូន​ប្រាសាក់ សុទ្ធ​តែ​ពិតប្រាកដ ត្រឹមត្រូវ និង​ថ្មីបំផុត។<br />
                    ២. ខ្ញុំ/យើងខ្ញុំ សូមបញ្ជាក់ថា បានទទួល​ បានអាន បានយល់ និង​ព្រម​អនុវត្ត​តាម​លក្ខខណ្ឌ​នៃ​ការ​ប្រើប្រាស់​​ផលិត​ផល និង​សេវាកម្ម​ផ្សេងៗ ដែលអាច​រកបាន​នៅ​ការិយាល័យ​ប្រាសាក់ និងវេបសាយត៍ <a href="https://www.prasac.com.kh" target="_blank">www.prasac.com.kh</a><br />
                    1. I/We declare that all information provided to PRASAC is complete, correct, true and up-to-date.<br />
                    2. I/We hereby confirm that I/We have recieved, read, understood and agreed to abide by all Terms and Conditions governing the use of Prodcuts and Services provided by PRASAC which have been made available at PRASAC office and at <a href="https://www.prasac.com.kh" target="_blank">www.prasac.com.kh</a>
                  </p>
                  ថ្ងៃ............ទី..........ឆ្នាំ............<br />
                  <div class="sub-label">
                    Day 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                    Month 
                    &nbsp;&nbsp;&nbsp;&nbsp; 
                    Year 
                  </div>
                  <br /><br /><br />
                  <div class="account_hold_wrapper">
                    <div class="account_hold"><p>ម្ចាស់គណនី<br />Account holder(1)</p></div>
                    <div class="account_hold"><p>ម្ចាស់គណនី<br />Account holder(2)</p></div>
                    <div class="account_hold"><p>ម្ចាស់គណនី<br />Account holder(3)</p></div>
                  </div>
                  <h1 class="khmer center">សម្រាប់ប្រាសាក់ ​​/ FOR PRASAC  USE ONLY</h1>
                  <div class="prasac_use_blog_wrapper">
                    <div class="prasac_use_blog center"><p>រៀបចំដោយ/Prepared by<br />ថ្ងៃ/Day........ខែ/Month.......ឆ្នាំ/Year.......</p></div>
                    <div class="prasac_use_blog center" style="border-left:1px solid #000000;"><p>ត្រួតពិនិត្យដោយ/Checked by<br />ថ្ងៃ/Day........ខែ/Month.......ឆ្នាំ/Year.......</p></div>
                    <div class="prasac_use_blog center" style="border-left:1px solid #000000;"><p>អនុម័តដោយ/Approved by<br />ថ្ងៃ/Day........ខែ/Month.......ឆ្នាំ/Year.......</p></div>
                  </div>
                </div>
              </div>
            </form>';
           $html.=' 
            <style>
              div.footer-wrapper{
                text-align:center;
                overflow:hidden;
                margin-top:-1.6em !important;
                width:80%
                display:block;
                float:right;
                padding-bottom:1em;
              }
              div.footer-wrapper div.box{
                width:75px;
                height:90px;
                float:right;
                padding-bottom:1.5em;
                border-bottom:1px dotted black;
                margin:0 1.4em;
              }
              div.box div.footer-write{
                  width:75px;
                  height:90px;
                  border:1px solid black;
              }
            </style>
          </body>';
      $mpdf->WriteHTML($html);
      if($td_date && $td_applicant_name && $td_account_name && $td_id_number){
        //==========TD Data post form==========
        $mpdf->AddPage('A4-L', // L - landscape, P - portrait 
          '', '', '',
          10, // margin_left
          10, // margin right
          10, // margin top
          10, // margin bottom
          10, // margin header
          10); // margin footer
          $mpdf->SetHTMLFooter('<div class="pdf-footer"><div class="footer-right">TD.001.V3​</div><div class="footer-left">Form : Term Deposit Account Application | Private Company </div></div>');
          date_default_timezone_set('Asia/Phnom_Penh');
          $date = new DateTime();
          $daysname = array('អាទិត្យ','ចន្ទ','អង្គារ','ពុធ','ប្រហស្បតិ៍','សុក្រ','សៅរ៍');
          $monthsname = array('ធ្នូ','មករា','គម្ភៈ','មីនា','មេសា','ឧសភា','មិថុនា','កក្ដា','សីហា','កញ្ញា','តុលា','វិច្ឆិកា');
          
          //==Daynum
          $daynum = 1;
          $dateObj   = DateTime::createFromFormat('!d', $daynum);
          // $current_daynum = $date->format('N');//Day index Monday is 1
          // echo "Day date=> ".date("d").'<br />';
          // echo "Day Index:=> ".$daysname[$current_daynum];
          
          $monthNum  = date("n",strtotime($td_date));
          $dateObj   = DateTime::createFromFormat('!m', $monthNum);
          $monthName = $dateObj->format('n'); // March
          $html_td .='
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
          <body>
          <head></head>
              <body>
                <style>
                  html,body{
                    font-family:hanuman,Verdana;
                    font-size:0.98em;
                  }
                  div.row{
                    width:100%;
                    margin:0 auto;
                    clear:both;
                    overflow:hidden;
                    padding:0;
                  }
                  div.container{
                    width:98%;
                    padding:0;
                    margin:0 auto;
                    overflow:hidden;
                    display:block;
                  }
                  div.col-10{
                    83.33333333%
                  }
                  div.col-8{
                    width:66.66666667%;
                  }
                  div.col-6{
                    width:50%;
                    float:left;
                  }
                  div.col-5{
                    width:41.66666667%%;
                    float:left;
                  }
                  div.col-4{
                    width:33.33333333%;
                    float:left;
                  }
                  div.col-3{
                    width:25%;
                    float:left;
                  }
                  div.col-12{
                    width:100%;
                    margin:0 auto;
                  }
                
                  div.khmer{
                    font-family:"khmerosmoul";
                    font-weight:lighter;
                    font-size:1.2em;
                  }
                  div.latin{
                    font-family:"Verdana";
                    font-weight:bold;
                    font-size:1.2em;
                  }
                  div.right{text-align:right;padding:0;margin:0;}
                  div.left{text-align:left;}
                  div.center{text-align:center;}
                  div.label{
                    display:block;
                    font-size:12px;
                    margin-top:-5px;
                  }
                  div.border-wrapper{
                    border:1px solid #000000;
                    padding:0px 10px;
                  }
                  div.border-wrapper div.left:nth-child(1){
                    padding:10px 0;
                  }
                  div.border-left{border-left:1px solid #000000;float:right;padding:10px;}
                  div.authorize-block{
                    margin-top:6em;
                    margin-right:4em;
                    overflow:hidden;
                  }
                  div.authorize-block p{
                    border-top:1px dotted #000000;
                    margin:0 15px;
                  }
                  div.data{font-weight: bold;}
            </style>
            <div class="row">
              <div class="col-6">
                <img src="../images/logo-website.png" alt="" title=""  width="100">
              </div>
              <div class="col-6">
                <div class="right khmer">​ពាក្យ​សុំ​បើក​គណនីបញ្ញើមានកាលកំណត់​</div>
                <div class="right latin">TERM DEPOSIT ACCOUNT APPLICATION</div>
              </div>
            </div>
            <div class="row">
              <div class="container">
                <div class="col-6">
                  <div class="col-4 left">​កាលបរិច្ឆេទ</div>
                  <div class="col-8 left">: ថ្ងៃ/Day: <span class="data">'.date("d",strtotime($td_date)).'</span>  ខែ/Month:  <span class="data">'.$monthsname[$monthNum].'</span>  ឆ្នាំ/Year: <span class="data">'.date("Y",strtotime($td_date)).' </span></div>
                  <div class="col-12 left label">Date</div>
                </div>
              </div>
              <div class="container">
                <div class="col-6">
                  <div class="col-4 left">ឈ្មោះអ្នកស្នើសុំ</div>
                  <div class="col-8 left">: '.$td_applicant_name.'</div>
                  <div class="col-12 left label">Applicant Name</div>
                </div>
                <div class="col-6">
                  <div class="col-4 left">លេខអត្តសញ្ញាណ</div>
                  <div class="col-8 left">: '.$td_id_number.'</div>
                  <div class="col-12 left label">ID Number</div>
                </div>
              </div>
              <div class="container">
                <div class="col-6">
                  <div class="col-4 left">ឈ្មោះ​គណនី</div>
                  <div class="col-8 left">: '.$td_account_name.' </div>
                  <div class="col-12 left label">Account Name</div>
                </div>
              </div>
              <div class="container">
                <div class="col-6">
                  <div class="col-4 left">គណនីទំនាក់ទំនង</div>
                  <div class="col-8 left">: '.$td_relate_account_number.'</div>
                  <div class="col-12 left label">Related Account No</div>
                </div>
                <div class="col-6">
                  <div class="col-4 left">លេខគណនីបញ្ញើមានកាលកំណត់</div>
                  <div class="col-8 left">: '.$td_account_number.'</div>
                  <div class="col-12 left label">Term Deposit Account Number</div>
                </div>
              </div>

              <div class="container">
                <div class="col-8">
                  <div class="col-3 left">ប្រភេទគណនី</div>
                  <div class="col-9 left">: ';
                  $matuity_growth_checked = $td_account_type==1?'checked="checked"':'';
                  $monthly_growth= $td_account_type==2?'checked="checked"':'';
                  $otherchecked = $td_account_type==3?'checked="checked"':'';

                  $khr_checked = $td_currency_type==1?'checked="checked"':'';
                  $usd_checked = $td_currency_type==2?'checked="checked"':'';
                  $thb_checked = $td_currency_type==3?'checked="checked"':'';
                  $other_checked = $td_currency_type==4?'checked="checked"':'';
                    $html_td .='
                      <input type="checkbox" '.$matuity_growth_checked.' name="td_account_type"  value="1"/> គណនីចម្រើនការប្រាក់​ចុងគ្រា 
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="checkbox" '.$monthly_growth.' name="td_account_type"  value="2"/> គណនីចម្រើនការប្រាក់ប្រចាំខែ 
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <input type="checkbox" '.$otherchecked.' name="td_account_type" value="3"/> ផ្សេងៗ';
                  $html_td.='
                  </div>
                  <div class="col-12 left label">
                    <div class="col-3 left"> Account Type</div>
                    <div class="col-9 left">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Maturity Growth Account 
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Monthly Growth Account 
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Others
                    </div>
                  </div>
                </div>
              </div>

              <div class="container">
                <div class="col-8">
                  <div class="col-3 left">ប្រភេទរូបិយប័ណ្ណ</div>
                  <div class="col-9 left">: ';
                    $html_td .='
                      <input type="checkbox" '.$khr_checked.' name="currency_type" value="1"/> ប្រាក់​រៀល 
                      &nbsp;&nbsp;
                      <input type="checkbox" '.$usd_checked.' name="currency_type"  value="2"/> ប្រាក់ដុល្លារអាមេរិក
                      &nbsp;&nbsp;
                      <input type="checkbox" '.$thb_checked.' name="currency_type"  value="3"/> ប្រាក់បាត
                      &nbsp;&nbsp;
                      <input type="checkbox"  '.$other_checked.' name="currency_type" value="4"/> ផ្សេងៗ';
                  $html_td.='
                  </div>
                  <div class="col-12 left label"> 
                    <div class="col-3 left">Currency Type</div>
                    <div class="col-9 left">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      KHR 
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      USD 
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      THB
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Others
                    </div>
                  </div>
                </div>
              </div>
              <div class="container">
                <div class="col-6">
                  <div class="col-4 left">ចំនួនទឹកប្រាក់បញ្ញើ</div>
                  <div class="col-8 left">: '.$td_deposit_amount.'</div>
                  <div class="col-12 left label">Deposit Amount</div>
                </div>
                <div class="col-6">
                  <div class="col-4 left">ចំនួនទឹកប្រាក់បញ្ញើជាអក្សរ</div>
                  <div class="col-8 left">: '.$td_deposit_amount_in_word.'</div>
                  <div class="col-12 left label">Deposit Amount in words</div>
                </div>
              </div>
              <div class="container">
                <div class="col-6">
                  <div class="col-4 left">​កាលបរិច្ឆេទដាក់ប្រាក់បញ្ញើ</div>
                  <div class="col-8 left">: ថ្ងៃ/Day:  '.date("l",strtotime($td_deposit_date)).'  ខែ/Month:  '.date("F",strtotime($td_deposit_date)).'   ឆ្នាំ/Year:  '.date("Y",strtotime($td_deposit_date)).'  </div>
                  <div class="col-12 left label">Deposit Date</div>
                </div>
                <div class="col-6">
                  <div class="col-4 left">រយៈពេលដាក់ប្រាក់បញ្ញើ</div>
                  <div class="col-8 left">: '.$td_term_of_deposit.'  ខែ</div>
                  <div class="col-4 left label">Term of Deposit</div>
                  <div class="col-4 label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Month</div>
                </div>
              </div>';
              $auto_roll_over = $td_td_condition==1?'checked="checked"':'';
              $close_on_maturity = $td_td_condition==0?'checked="checked"':'';

              $td_roll_over_pi =$td_roll_over_type==1?'checked="checked"':'';
              $td_roll_over_po = $td_roll_over_type==2?'checked="checked"':'';
              $td_roll_over_sa = $td_roll_over_type==3?'checked="checked"':'';
              $td_roll_over_sa_amount = $td_roll_over_sa_amount?$td_roll_over_sa_amount:'';
              $html_td .='
              <div class="container">
                <div class="col-6 left">
                  <input type="checkbox" '.$auto_roll_over.'  name="td_td_condition" value="1"/>​ <b>បន្ដដោយស្វ័យប្រវត្តិ / AUTO ROLL OVER</b>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="checkbox" '.$close_on_maturity.' name="td_td_condition" value="0"/> <b>​មិនបន្ដ / CLOSE ON MATURITY</b>
                </div>
              </div>

              <div class="container border-wrapper">
                <div class="col-6 left">
                  <input type="checkbox" '.$td_roll_over_pi.' name="auto_roll_over" value="1"/>​ បន្ដទាំងប្រាក់ដើមនិងប្រាក់ការ/Roll Over Principal and Interest<br />
                  <input type="checkbox" '.$td_roll_over_po.' name="auto_roll_over" value="2"/>​ បន្ដតែប្រាក់ដើម/Roll Over Principal​ Only<br />
                  <input type="checkbox" '.$td_roll_over_sa.' name="auto_roll_over" value="3"/>​ បន្ដតែប្រាក់ចំនួន/Roll Over Specific Amount:  '.$td_roll_over_sa_amount.'
                </div>
                <div class="left border-left">
                  យើង​ខ្ញុំ​បាន​អាន​ និង​យល់​ព្រម​ប្រតិបត្តិ​ទៅតាម​គោលការណ៍និង​លក្ខខណ្ឌនៃ​គណនីមានកាលកំណត់របស់ប្រាសាក់។ I have read and agreed to comply with terms and conditions of Term Deposit of PRASAC.';
                  $html_td .='
                  <div class="col-8" style="margin:0 auto;border-top:1px dotted #000000;margin-top:2.5em;text-align:center;">';
                    $html_td .="<p style='margin:0 auto;'>អ្នក​ស្នើសុំ<br />Applicant's signature</p>";
                    $html_td .='
                  </div>
                </div>
              </div>

              <div class="container">
                <div class="col-8 right authorize-block">
                  <div class="col-4 center"><p>រៀបចំដោយ/Prepared By</p></div>
                  <div class="col-4 center"><p>ត្រួតពិនិត្យដោយ/Checked By</p></div>
                  <div class="col-4 center"><p>អនុម័តដោយ/Approved By</p></div>
                </div>
              </div>

            </div>
          </body>
        </html>';
        // echo $html_td;
        $mpdf->WriteHTML($html_td);
      }//======End if here==========
    }//========End while here==========
?>