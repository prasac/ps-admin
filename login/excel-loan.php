<?php 
$objPHPExcel->setActiveSheetIndex(0);
$stylehead = array(
  'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
  ),
  'font'  => array(
      'bold'  => true,
      'color' => array('rgb' => '4DB848'),
      'size'  => 18,
      'name'  => 'Khmer OS Muol Light'
  )
);

$style = array(
  'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
  ),
  'font'  => array(
      'bold'  => true,
      'color' => array('rgb' => 'ffffff'),
      'size'  => 10,
      'name'  => 'Khmer OS Content'
  ),
  'borders' => array(
    'allborders' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('rgb' => '449d44')
    )
    ),
);

$stylecontent = array(
  'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
  ),
  'font'  => array(
      'bold'  => false,
      'size'  => 10,
      'name'  => 'Khmer OS Content'
  ),
  'borders' => array(
    'allborders' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('rgb' => '449d44')
    )
    ),
);
$stylecontentright = array(
  'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
  ),
  'font'  => array(
      'bold'  => false,
      'size'  => 10,
      'name'  => 'Khmer OS Content'
  ),
  'borders' => array(
    'allborders' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('rgb' => '449d44')
    )
),
);


$objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->setCellValue('A1','របាយការណ៍​​ឈ្មោះ​អតិថិជន​ស្នើរ​សុំ​ប្រាក់​កម្ចី​​');
$objPHPExcel->getActiveSheet()->getStyle("A1:L1")->applyFromArray($stylehead);
$objPHPExcel->getActiveSheet()->mergeCells('A2:D2');
$objPHPExcel->getActiveSheet()->mergeCells('F5:H5');
$objPHPExcel->getActiveSheet()->mergeCells('A5:A6');
$objPHPExcel->getActiveSheet()->mergeCells('B5:B6');
$objPHPExcel->getActiveSheet()->mergeCells('C5:C6');
$objPHPExcel->getActiveSheet()->mergeCells('D5:D6');
$objPHPExcel->getActiveSheet()->mergeCells('E5:E6');
$objPHPExcel->getActiveSheet()->mergeCells('I5:I6');
$objPHPExcel->getActiveSheet()->mergeCells('J5:J6');
$objPHPExcel->getActiveSheet()->mergeCells('K5:K6');
$objPHPExcel->getActiveSheet()->mergeCells('L5:L6');


$objPHPExcel->getActiveSheet()->setCellValue('A2','Date: '.$current_date);
$objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(25);

$rowCount = 5;
$customTitle = array ('លេខ','ឈ្មោះអតិថិជន​','ភេទ​','លេខ​ទូរស័ព្ទ​','អសយដ្ឋាន','ទំហំទឹកប្រាក់​ខ្ចី','រយៈពេល(ខែ)','របៀបសង','ថ្ងៃ​ស្នើ​សុំ​','សាខា​ស្នើ​សុំ​កម្ចី​');
$colName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O');




$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $customTitle[0]);
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $customTitle[1]);
$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $customTitle[2]);
$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $customTitle[3]);
$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $customTitle[4]);
$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $customTitle[5]);
$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $customTitle[6]);
$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $customTitle[7]);
$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $customTitle[8]);
$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $customTitle[9]);


$objPHPExcel->getActiveSheet()->SetCellValue('F6','រៀល');
$objPHPExcel->getActiveSheet()->SetCellValue('G6','ដុល្លាអាមេរិក');
$objPHPExcel->getActiveSheet()->SetCellValue('H6','បាត');


$objPHPExcel->getActiveSheet()->getStyle("A5:L5")->applyFromArray($style);
$objPHPExcel->getActiveSheet()->getStyle('A5:L5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('4DB848');

$objPHPExcel->getActiveSheet()->getStyle("A6:L6")->applyFromArray($style);
$objPHPExcel->getActiveSheet()->getStyle('A6:L6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('4DB848');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(80);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);


$query = "SELECT 
c.id,c.customer_gender,c.customer_dob,c.customer_personal_identify,c.customer_personal_number,c.borrow_amount_character,c.borrow_duration,c.payment_method,
c.customer_name,FORMAT(c.borrow_amount,2) as borrow_amount,c.brrow_rate_request_permonth,c.customer_address,c.customer_street,c.customer_group,v.name_kh as customer_village,
c.customer_phone,c.customer_status,co.name_kh as customer_commune,d.name_kh as customer_district,pv.name_kh as customer_province,c.business_type,c.business_purpose,
b.name_kh as branch_name,c.created_date
FROM loan_customer AS c 
left join villages as v on c.customer_village=v.id
left join communes as co on c.customer_commune=co.id 
left join districts as d on c.customer_district=d.id 
left join provinces as pv on c.customer_province=pv.id
left join branches as b on c.duty_station=b.id
where c.id in($items_checked)
ORDER BY c.id ASC ";
$result = $conn->query($query);
$customer  = array();

$businessType = array('កសិកម្ម​','ពាណិជ្ជកម្ម/ជំនួញ','សេវាកម្ម​​','ដឹកជញ្ជួន','សាងសង់','ប្រើប្រាស់​ទូទៅក្នុងគ្រួសារ');
$businessPurpose = array('​ឯកកម្ម​សិទ្ធិ','សហកម្មសិទ្ធិ','ពង្រីកមុខជំនួញ','ចាប់​ផ្ដើម​មុខ​ជំនួញ');
$personal_identify = array(1=>'​សៀវភៅ​គ្រួសារ​​',2=>'អត្ត​សញ្ញាណ​ប័ណ្ណ​',3=>'លិខិត​ឆ្លង​ដែន​',4=>'សំបុត្រ​កំណើត​',5=>'ប័ណ្ណ​បើកបរ​');
$relationship = array(1=>'ប្ដី',2=>'ប្រពន្ធ',3=>'បង',4=>'ប្អូន',5=>'ម្ដាយ',6=>'ឪពុក',7=>'ផ្សេងៗ');
$payment_method = array(1=>'ប្រ​ចាំ​ខែ​',2=>'ប្រចាំ​ត្រី​មាស​',3=>'ប្រ​ចាំ​ឆមាស​',4=>'ប្រចាំ​ឆ្នាំ​');

if($result->num_rows){
  $n=7;
  $i=1;
  while($row = mysqli_fetch_assoc($result)) {
    //   var_dump($row);
    //   exit;
    $id = $row['id'];
    $customer_name = $row['customer_name'];
    $customer_gender = ($row['customer_gender'] && $row['customer_gender']==1)?'ប្រុស':'ស្រី';
    $customer_dob = date('d-m-Y',strtotime($row['customer_dob']));
    $customer_personal_identify = $personal_identify[$row['customer_personal_identify']];
    $customer_personal_number = $row['customer_personal_number'];
    $customer_phone = $row['customer_phone'];
    $borrow_amount = $row['borrow_amount'];
    $customer_address = $row['customer_address'];
    $customer_street = $row['customer_street'];
    $customer_group = $row['customer_group'];
    $customer_village = $row['customer_village'];
    $customer_commune = $row['customer_commune'];
    $customer_district = $row['customer_district'];
    $customer_province = $row['customer_province'];
    $customer_full_addresss = 'ផ្ទះ​លេខ​ '.$customer_address.'ផ្លូវលេខ​ '.$customer_street.' ក្រុម​ទី'.$customer_group.' ភូមិ​ '.$customer_village.' ​ឃុំ/សង្កាត់ '.$customer_commune.' ស្រុក/ខណ្ឌ​ '.$customer_district.' ខេត្ត​/ក្រុង '.$customer_province;
    $business_purpose = $row['business_purpose'];
    $borrow_amount_character = $row['borrow_amount_character'];
    $borrow_duration = $row['borrow_duration'];
    $brrow_rate_request_permonth = $row['brrow_rate_request_permonth'];
    $branch_name = trim($row['branch_name']);
    $payment_request_method = $payment_method[$row['payment_method']];
    $request_date = $row['created_date'];

    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$n,$i);
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$n,$customer_name);
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$n,$customer_gender);
    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$n,$customer_phone);
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$n,$customer_full_addresss);
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$n,$borrow_amount);
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$n,$borrow_amount);
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$n,$borrow_amount);
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$n,$borrow_amount);
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$n,$borrow_duration);
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$n,$payment_request_method);
    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$n,date('d F Y',strtotime($request_date)));
    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$n,$branch_name);

    $objPHPExcel->getActiveSheet()->getStyle("A".$n.":L".$n)->applyFromArray($stylecontent);
    $objPHPExcel->getActiveSheet()->getStyle("F".$n.":I".$n)->applyFromArray($stylecontentright);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$n.':'.'L'.$n)->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getRowDimension($n)->setRowHeight(28);
    $objPHPExcel->getActiveSheet()->getStyle("A".$n)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
    $objPHPExcel->getActiveSheet()->getStyle("A".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
    $objPHPExcel->getActiveSheet()->getStyle("A".$n.":"."L".$n)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
    
    $rowCount++;
    $n++;
    $i++;
  }
}
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
ob_end_clean();
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="Report_Load_Request.xlsx"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
header('Cache-Control: max-age=0'); //no cache
$objWriter->save('php://output');
$message = '<span style="color:#4DB848;">Data have been successful exported.</span>';
exit;
?>