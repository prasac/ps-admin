<?php
  include('session.php');
  if(!isset($_SESSION['login_user'])){
    header("location:index.php");
    exit();
  }
  
  include('header.php');
  $header_text = 'ប្រព័ន្ធគ្រប់គ្រងសំណើប្រាក់កម្ចី';
  $service_type = 'loan';
  if((isset($_POST['btn-select']) || isset($_POST['service_type'])) && $_POST['service_type']=='deposit'){
    $header_text = 'ប្រព័ន្ធគ្រប់គ្រងសំណើប្រាក់បញ្ញើសន្សំ';
    $service_type = 'deposit';
  }
  include('head.php');

  if(isset($_POST['btn-pdf'])){
    include('pdf.php');
  }
  if(isset($_POST['btn-excel'])){
    include('excel.php');
  }
  if(isset($_POST['btn-unread']) || isset($_POST['btn-read'])){
    $status = isset($_POST['btn-unread'])?0:1;
    include('setting.php');
  }
  ?>
  <div class="row">
    <div class="col s12 m12 l12">
      <form class="form service-type" action="" method="POST">
        <div class="row">
          <div class="input-field col s4 l3 left">
            <select name="service_type" class="browser-default waves-effect">
              <option value="loan" <?php echo isset($_POST['service_type']) &&  $_POST['service_type']=='loan'?'selected':''?>>គ្រប់គ្រងសម្នើរប្រាក់កម្ចី</option>
              <option value="deposit" <?php echo isset($_POST['service_type']) &&  $_POST['service_type']=='deposit'?'selected':''?>>គ្រប់គ្រងសម្នើរប្រាក់បញ្ញើសន្សំ</option>
            </select>
          </div>
          <div class="input-field col s6 l1 left">
              <button class="btn waves-effect waves-light tooltipped" type="submit" name="btn-select" data-delay="30" data-tooltip="ជ្រើសរើសប្រតបត្តិការ">ជ្រើសរើស
              </button>
          </div>
        </div>
      </form>
      <form class="form search" action="" method="POST">
        <input type="hidden" value="<?php echo isset($_POST['service_type'])?$_POST['service_type']:''?>" name="service_type"/>
        <div class="row">
          <div class="input-field col s4 l3 left"></div>
          <div class="input-field col s4 l2 left">
            <select name="province" id="province" class="browser-default waves-effect" required="" aria-required="true">
              <option value="" disabled selected>ជ្រើសរើស​ទីតាំងសាខា</option>
              <?php
              $query_branch = $conn->query("select id,name_kh from provinces order by name_kh");
              $province_posted = isset($_POST['province'])?$_POST['province']:false;
                $i=0;
                while($row = mysqli_fetch_object($query_branch)) {
                  if($province_posted && $province_posted==$row->id){
                    echo '<option value="'.$row->id.'" selected>'.$row->name_kh.'</option>';
                  }else{
                    echo '<option value="'.$row->id.'">'.$row->name_kh.'</option>';
                  }
                  $i++;
                }
              ?>
            </select>
          </div>

          <div class="input-field col s4 l2 left">
            <select name="branch" id="branch" class="browser-default waves-effect" required="" aria-required="true">
              <option value="" disabled selected>ជ្រើសរើស​សាខា</option>
              <?php
              $query_branch = $conn->query("select id,name_kh,province_id from branches order by id");
              $branch_posted = isset($_POST['branch'])?$_POST['branch']:false;
                $i=0;
                while($row = mysqli_fetch_object($query_branch)) {
                  if($branch_posted && $branch_posted==$row->id){
                    echo '<option value="'.$row->id.'" selected class="'.$row->province_id.'">'.$row->name_kh.'</option>';
                  }else{
                    echo '<option value="'.$row->id.'" class="'.$row->province_id.'">'.$row->name_kh.'</option>';
                  }
                  $i++;
                }
              ?>
            </select>
          </div>
          <div class="input-field col s6 l2 left">
            <input type="date" class="datepicker  required" required="" aria-required="true" id="beginDate" name="start_date" data-value="<?php echo isset($_POST['start_date'])?$_POST['start_date']:'';?>"/>
            <label class="<?php echo isset($_POST['start_date'])?'active':'';?>" for="beginDate" data-error="ត្រូវតែបំពេញ!">​ពី​</label>
          </div>
          <div class="input-field col s6 l2 left">
            <input type="date" class="datepicker required" required="" aria-required="true" id="endDate" name="end_date" data-value="<?php echo isset($_POST['end_date'])?$_POST['end_date']:'';?>"/>
            <label class="<?php echo isset($_POST['end_date'])?'active':'';?>" for="endDate" data-error="ត្រូវតែបំពេញ!">ដល់​</label>
          </div>

          <div class="input-field col s6 l1 left">
              <button class="btn waves-effect waves-light tooltipped" type="submit" name="btn-search" data-delay="30" data-tooltip="ស្វែងរកអតិថិជន">ស្វែងរក
                <i class="material-icons right">search</i>
              </button>
          </div>
        </div>
      </form>
    </div>
    <?php
     
    if(isset($_POST['service_type']) && $_POST['service_type']=='deposit'){
      include('deposit-list.php');
    }else{
      include('loan-list.php');
    }
    ?>
  </div>
  <?php
  include('footer.php');
  ?>