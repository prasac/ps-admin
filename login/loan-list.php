
     

  <div class="col s12 l12">
      <form class="form" action="" method="POST">
        <input type="hidden" value="loan" name="service_type" />
        <table class="highlight bordered">
          <thead>
            <tr>
                <th colspan="3">
                    <span class="left">ទាញយក​ទិន្នន័យជា៖</span>
                    <button type="submit" name="btn-pdf" class="left tooltipped" style="padding:0 8px;background:transparent;border:none;" data-delay="30" data-tooltip="ទាញយក​ទិន្នន័យជា​ PDF"><i class="fa fa-file-pdf-o fa-2x btn-pdf" aria-hidden="true"></i></button> 
                    <button type="submit" name="btn-excel" class="left tooltipped" style="padding:0 8px;background:transparent;border:none;" data-delay="30" data-tooltip="ទាញ​យក​ទិន្នន័យ​ជា EXCEL"><i class="fa fa-file-excel-o fa-2x btn-excel" aria-hidden="true"></i></button> 
                </th>
                <!-- <th colspan="1">
                    <span class="left">យល់​ព្រម​៖</span>
                    <button type="submit" name="btn-approved" class="left tooltipped approved" style="padding:0 8px;background:transparent;border:none;" data-delay="30" data-tooltip="បដិសេធ​សំណើរ​"><i class="small material-icons prefix">clear</i></button> 
                </th>
                <th colspan="1">
                    <span class="left">បដិសេធ៖</span>
                    <button type="submit" name="btn-rejected" class="left tooltipped rejected" style="padding:0 8px;background:transparent;border:none;" data-delay="30" data-tooltip="បដិសេធ​សំណើរ​"><i class="small material-icons prefix">clear</i></button> 
                </th> -->
                <th colspan="3" style="text-align:left;padding:0 5px;">
                    <?php echo isset($message)?$message:''; ?>
                </th>
                <td>
                    <button type="button" name="btn-approved" class="right approved dropdown-button" style="padding:0 8px;background:transparent;border:none;" data-beloworigin="true" data-activates='dropdown1'><i class="small material-icons prefix">settings</i></button>    
                    <!-- Dropdown Structure -->
                  <ul id='dropdown1' class='dropdown-content'>
                    <li><button type="submit" name="btn-unread" style="padding:4px;background:transparent;border:none;"> មិនទាន់មើល</button></li>
                    <li><button type="submit" name="btn-read" style="padding:4px;background:transparent;border:none;"> មើលរួច</button></li>
                  </ul>
                </td>
                
            </tr>
            <tr>
                <th class="centered">
                    <input type="checkbox" id="check-all" name="check_all"/>
                    <label for="check-all"></label>
                </th>
                <th class="centered">ឈ្មោះ​អតិថិជន</th>
                <th class="centered">ទឹិក​ប្រាក់​សុំ​ចងការ​</th>
                <th class="centered">ប្រភេទ​មុខ​ជំនួញ​</th>
                <th class="centered">ឈ្មោះសាខា</th>
                <th class="centered">អាសយដ្ឋាន</th>
                <th class="centered">លេខ​ទូរស័ព្ទ​</th>
                <!-- <th class="centered">ស្ថាន​ភាព​កម្ចី​</th> -->

            </tr>
          </thead>
          <tbody>
        
          <?php
              
            $limit = 50;
            $offset = isset($_GET['page'])?$_GET['page']:0;
            if (isset($_GET["page"])) { 
              $page  = $_GET["page"]; 
              } else { 
                $page=1; 
              };  
              
            $start_from = ($page-1) * $limit; 
            $businessType = array('កសិកម្ម​','ពាណិជ្ជកម្ម/ជំនួញ','សេវាកម្ម​​','ដឹកជញ្ជួន','សាងសង់','ប្រើប្រាស់​ទូទៅក្នុងគ្រួសារ');
            $businessPurpose = array('​ឯកកម្ម​សិទ្ធិ','សហកម្មសិទ្ធិ','ពង្រីកមុខជំនួញ','ចាប់​ផ្ដើម​មុខ​ជំនួញ');

            $condition = '';
            $pagination = " LIMIT $start_from, $limit";
            if(isset($_POST['btn-search'])){
              $start_date = $_POST['start_date']?date("Y-m-d",strtotime($_POST['start_date'])):'';
              $end_date = $_POST['end_date']?date("Y-m-d",strtotime($_POST['end_date'])):'';
              $branch = isset($_POST['branch'])?$_POST['branch']:'';//implode(',',$_POST['branch']):'';
              $province =  isset($_POST['province'])?$_POST['province']:'';
              //  var_dump( $branch);
                if($province){
                  $condition .= " c.customer_province=$province";
                }
                if($branch){
                  $condition .= " and  c.duty_station in($branch)";
              }

                
              if($start_date && !$end_date){
                  $condition .= " c.created_date >= '$start_date'";
              }
              if($end_date && !$start_date){
                  $condition .= " c.created_date >= '$end_date'";
              }
              if($end_date && $start_date){
                  $condition .= " c.created_date between '$start_date' and '$end_date'";
              }

            
              if($start_date || $end_date || $branch || $province){
                $condition = " where $condition";
              }
              $pagination = "";

            }
            


            $query = "SELECT 
                  c.id,
                  c.customer_name,FORMAT(c.borrow_amount,2) as borrow_amount,c.brrow_rate_request_permonth,c.customer_address,c.customer_street,c.customer_group,v.name_kh as customer_village,
                  c.customer_phone,c.customer_status,co.name_kh as customer_commune,d.name_kh as customer_district,pv.name_kh as customer_province,c.business_type,c.business_purpose,
                  b.name_kh as branch_name,c.is_view
                  FROM loan_customer AS c
                  left join villages as v on c.customer_village=v.id
                  left join communes as co on c.customer_commune=co.id 
                  left join districts as d on c.customer_district=d.id 
                  left join provinces as pv on c.customer_province=pv.id
                  left join branches as b on c.duty_station=b.id
                  $condition
                  ORDER BY b.name_kh ASC $pagination";
            $result = $conn->query($query);
            //  echo $query;
            $customer  = array();
            if(($result) && $result->num_rows>0){
              while($row = mysqli_fetch_object($result)) {
                $is_view = $row->is_view?'ready':'not-ready';
                ?>
                  <tr class="<?php echo $is_view;?>">
                    <td class="right">
                        <input type="checkbox" id="checkbox-item<?php echo $row->id ?>" name="checkboxitem[]" value="<?php echo $row->id ?>"/>
                        <label for="checkbox-item<?php echo $row->id ?>"></label>
                    </td>
                    <td class="detail modal-trigger"  id="<?php echo base64_encode($row->id);?>"><?php echo $row->customer_name ?></td>
                    <td class="detail"  id="<?php echo base64_encode($row->id);?>">$<?php echo $row->borrow_amount ?></td>
                    <td class="detail"  id="<?php echo base64_encode($row->id);?>">
                        <?php 
                          // echo $businessType[$row->business_type];
                          $business_type_explode = explode(',',$row->business_type);
                          if(count($business_type_explode)){
                            for($i=0;$i<count($business_type_explode);$i++){
                              // echo $businessType[$business_type_explode[$i]];
                              echo '<input type="checkbox" name="service" value="service">​' .$businessType[$business_type_explode[$i]];
                              echo ($i+1<count($business_type_explode))?', ':'';
                            }
                          }
                        ?>​
                    </td>
                    <td class="detail"  id="<?php echo base64_encode($row->id);?>"><?php echo $row->branch_name; ?></td>
                    <td class="detail"  id="<?php echo base64_encode($row->id);?>">
                        <?php echo 'ផ្ទះ​លេខ​ '.$row->customer_address.'&nbsp;&nbsp;ផ្លូវលេខ​ '.$row->customer_street.'&nbsp;&nbsp;ក្រុម​ទី'.$row->customer_group.'&nbsp;&nbsp;'.$row->customer_village.'&nbsp;​'.$row->customer_commune.'&nbsp;'.$row->customer_district.'&nbsp;'.$row->customer_province;?>
                    </td>
                    <td class="detail"  id="<?php echo base64_encode($row->id);?>"><?php echo $row->customer_phone ?></td>
                    <!-- <td> -->
                      <?php 
                        switch($row->customer_status){
                            case 1:
                              // $status = '<div class="preloader-wrapper small active tooltipped" data-position="right" data-delay="50" data-tooltip="កំពុង​ដំណើរ​ការ..."><div class="spinner-layer spinner-green-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>';
                              $status = '<div class="small active tooltipped in-progress" data-position="right" data-delay="50" data-tooltip="កំពុង​ដំណើរ​ការ...">កំពុង​ដំណើរ​ការ...</div>';
                            break;
                            case 2:
                              // $status = '<div class="tooltipped" data-delay="30" data-tooltip="យល់​ព្រម​" data-position="right" ><i class="small material-icons prefix active">done</i></div>';
                              $status = '<div class="small active tooltipped approved" data-position="right" data-delay="50" data-tooltip="យល់​ព្រម​">យល់​ព្រម​</div>';
                            break;
                            case 3:
                              // $status = '<div class="tooltipped" data-delay="30" data-tooltip="បដិសេធ" data-position="right" ><i class="small material-icons prefix cancel">clear</i></div>';
                              $status = '<div class="small active tooltipped rejected" data-delay="30" data-tooltip="បដិសេធ" data-position="right" >បដិសេធ</div>';
                            break;
                            default:
                              $status ='';
                            break;
                        }
                        //echo $status;
                      ?>
                    <!-- </td> -->
                  </tr>
              <?php
              }
            }else{
              echo '<tr><td class="centered" colspan="8" style="color:#ee6e73;text-align:center;">ពុំ​មាន​ទិិន្នន័យ​ដែល​លោក​អ្នក​ស្វែង​រក​​នោះ​ទេ.</td></tr>';
            }

            // if (ob_get_level() == 0) ob_start();
            
            // for ($i = 0; $i<10; $i++){
            
            //         echo "<br> Line to show.";
            //         echo str_pad('',4096)."\n";    
            
            //         ob_flush();
            //         flush();
            //         sleep(2);
            // }
            
            // echo "Done.";
            
            // ob_end_flush();

            
            // $total_stuffs = 200;
            // $current_stuff = 0;
            // while ($current_stuff < $total_stuffs) {
            //   $progress = round($current_stuff * 100 / $total_stuffs, 2);
          
            //   // ... some stuff
            //   sleep(1);
          
            //   $current_stuff++;
            // }
            ?>
            </tbody>
            <?php 
            $pagequery = "SELECT count(*) FROM loan_customer AS cu INNER JOIN loan_purpose AS pu ON cu.id=pu.customer_id";
            $pageresult = $conn->query($pagequery);
            $row = mysqli_fetch_row($pageresult);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit); 
            //if no page var is given, default to 1.
            $prev = $page - 1;							//previous page is page - 1
            $next = $page + 1;							//next page is page + 1
            $lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
            $lpm1 = $lastpage - 1;						//last page minus 1
            $activepre = $prev==0?'disabled':'waves-effect';
            $prevlink = $prev?"?page=$prev":'#';

            $activenext = $next>$total_pages?'disabled':'waves-effect';
            $nextlink = $next<=$total_pages?"?page=$next":'#';

              if( $total_records>$limit){?>
              <tfoot>
                  <tr>
                    <td colspan="9" class="centered">
                      <ul class="pagination">
                        <li class="<?php echo $activepre;?>"><a href="<?php echo $prevlink;?>"><i class="material-icons">chevron_left</i></a></li>
                        <?php
                          for ($i=1; $i<=$total_pages; $i++) { 
                          $active = (isset($_GET['page']) && $_GET['page']==$i) || (!isset($_GET['page']) && $i==1)?'active':'';
                          ?>
                          <li  class="<?php echo $active;?> waves-effect"><a href="?page=<?php echo $i;?>"><?php echo $i;?></a></li>
                          <?php 
                          }
                        ?>
                        <li class="<?php echo $activenext;?>"><a href="<?php echo $nextlink;?>"><i class="material-icons">chevron_right</i></a></li>
                      </ul>
                    </td>
                  </tr>
              </tfoot>
              <?php 
              }
              $conn->close();
            ?>
        </table>
      </form>
    </div>