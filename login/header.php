<!DOCTYPE html>
<html>
  <head>
    <title>Loan Request Management</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/css?family=Nokora" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="../library/font-awesome/css/font-awesome.min.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="../css/style.css" media="screen" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://www.prasac.com.kh/wp-content/uploads/2017/04/prasac_logo_favicon_favoicon.png" rel="shortcut icon">
  </head>
  <body>