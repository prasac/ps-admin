    <div class="footer-wrapper">
        <footer class="page-footer">
            <div class="footer-copyright">
                © <?php echo date("Y");?> All Rights Reserved PRASAC​ Microfinance Institution
                <a class="grey-text text-lighten-4 right" href="https://www.prasac.com.kh" target="_blank">www.prasac.com.kh</a>
            </div>
        </footer>
    </div>
</div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- jQueryValidation Plugin (optional) -->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
    <!--Import Materialize-Stepper JavaScript -->
    <script src="../library/materialize/js/materialize-stepper.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
    <script type="text/javascript" src="../library/jquery.chained.js"></script>
    <script>
         $("#branch").chained("#province");
      </script>

    <script>
        $(document).ready(function() {

            $("td.detail").click(function(){
              var id = $(this).attr("id");
              window.open('detail.php?id='+id,"_self");
            });

            Materialize.updateTextFields();
            $(".button-collapse").sideNav();
            $('.tooltipped').tooltip({delay: 50});

            $('select').material_select();

            // function checkDate(e) {
                
            // }

            // $('form.search').submit(function() {
            //     if ($('#beginDate').val() == '' || $('#endDate').val() == '') {
            //         $('#beginDate,#endDate').addClass('invalid');
            //         return false;
            //     }else {
            //         $('#beginDate').removeClass('invalid');
            //         $('#endDate').removeClass('invalid');
            //         return true;
            //     }
                
            // });

            // $('#beginDate').change(function() {
            //     checkDate();
            // });
        });
        
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrainWidth: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left', // Displays dropdown with edge aligned to the left of button
            stopPropagation: false // Stops event propagation
            }
        );
        
        $('.datepicker').pickadate({
            format: 'd mmmm, yyyy',
            selectMonths: true, // Creates a dropdown to control month
            selectYears:65, // Creates a dropdown of 15 years to control year
            firstDay: 1,
            closeOnSelect:true,
            editable:false,
            // monthsFull: [ 'មករា', 'កុម្ភៈ', 'មីនា', 'មេសា', 'ឧសភា', 'មិថុនា', 'កក្កដា', 'សីហា', 'កញ្ញា', 'តុលា', 'វិច្ឆិកា', 'ធ្នូ' ],
            // monthsShort:[ 'មករា', 'កុម្ភៈ', 'មីនា', 'មេសា', 'ឧសភា', 'មិថុនា', 'កក្កដា', 'សីហា', 'កញ្ញា', 'តុលា', 'វិច្ឆិកា', 'ធ្នូ' ],
            weekdaysFull: [ 'អាទិត្យ', 'ចន្ទ', 'អង្គារ', 'ពុធ', 'ព្រហស្បត៌​', 'សុក្រ​', 'សៅរ៍' ],
            weekdaysShort: [ 'អាទិត្យ', 'ចន្ទ', 'អង្គារ', 'ពុធ', 'ព្រហស្បត៌​', 'សុក្រ​', 'សៅរ៍' ],
            weekdaysLetter: [ 'អ', 'ច', 'អ', 'ព', 'ព្រ', 'ស', 'ស' ],
            today: 'ថ្ងៃនេះ​',
            clear: 'សំអាត',
            close: 'បិទ',
            labelMonthNext: 'ខែ​បន្ទាប់​',
            labelMonthPrev: 'ខែ​មុន​',
            labelMonthSelect: 'ជ្រើស​រើស​ខែ',
            labelYearSelect: '​ជ្រើស​រើស​ឆ្នាំ​',
            max:new Date(),
        });

        var picker = $('#beginDate').pickadate('picker');
        var picker = $('#endDate').pickadate('picker');
        
        $('#check-all').on('change', function() {
            $('tbody tr td input').prop('checked', this.checked);
        });

    </script>
</body>
</html>